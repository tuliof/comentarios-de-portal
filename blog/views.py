from django.shortcuts import render
from django.http import HttpResponse

from .models import Entry

# Create your views here.

def index(request):
	entries_list = Entry.objects.order_by('-pub_date')
	context = {'entries_list': entries_list}
	return render(request, 'blog/index.html', context)