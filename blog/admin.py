from django.contrib import admin

# Register your models here.
from .models import Entry, Site, Author

class EntryAdmin(admin.ModelAdmin):
	#Altera a ordem dos campos no form do admin
	fields = ['text', 'news_title', 'news_link', 'pub_date']
	#O que será exibido na tabela do admin
	list_display = ('news_title', 'news_link', 'text', 'pub_date')
	search_fields = ['news_title', 'text']

class SiteAdmin(admin.ModelAdmin):
	list_display = ('name')

admin.site.register(Entry)
admin.site.register(Site)
admin.site.register(Author)