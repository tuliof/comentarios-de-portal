from django.db import models

# Create your models here.

class Site(models.Model):
	short_name = models.CharField(max_length=50)
	name = models.CharField(max_length=50)

	def __str__(self):
		return self.name

class Author(models.Model):
	name = models.CharField(max_length=255)

	def __str__(self):
		return self.name

class Entry(models.Model):
	text = models.TextField(blank=False)
	entry_date = models.DateTimeField('Data do comentário')
	author_name = models.ForeignKey(Author)
	pub_date = models.DateTimeField('Data da publicação', auto_now_add=True)
	news_title = models.CharField(max_length=255)
	news_link = models.CharField(max_length=255)
	news_site = models.ForeignKey(Site)
	publisher_comment = models.TextField(blank=True)
	# rating system # rating = models.IntegerField(default=0)

	def __str__(self):
		return self.news_title
